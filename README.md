# elm-gameboy

Experimental Gameboy emulation written in Elm, hence the name.

## References

  * http://imrannazar.com/GameBoy-Emulation-in-JavaScript:-The-CPU
  * Probably need some Z80 stuff for extra information.