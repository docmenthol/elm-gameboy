module Z80.CPU exposing (..)

import Bitwise
import Z80
    exposing
        ( CPU
        , Clock
        , Opcode(..)
        , Register(..)
        , Registers
        )


tick : CPU -> CPU
tick cpu =
    let
        clock =
            Clock (cpu.clock.m + cpu.registers.m) (cpu.clock.t + cpu.registers.t)

        registers =
            resetRegisters [ M, T ] cpu.registers
    in
    CPU clock registers


writeTick : Registers -> Registers
writeTick =
    writeRegisters [ M, T ] [ 1, 4 ]


writeRegister : ( Register, Int ) -> Registers -> Registers
writeRegister ( r, v ) set =
    case r of
        A ->
            { set | a = v }

        B ->
            { set | b = v }

        C ->
            { set | c = v }

        D ->
            { set | d = v }

        E ->
            { set | e = v }

        H ->
            { set | h = v }

        L ->
            { set | l = v }

        F ->
            { set | f = v }

        PC ->
            { set | pc = v }

        SP ->
            { set | sp = v }

        M ->
            { set | m = v }

        T ->
            { set | t = v }


writeRegisters : List Register -> List Int -> Registers -> Registers
writeRegisters rs vs set =
    List.map2 Tuple.pair rs vs
        |> List.foldl writeRegister set


resetRegister : Register -> Registers -> Registers
resetRegister r =
    writeRegister ( r, 0 )


resetRegisters : List Register -> Registers -> Registers
resetRegisters rs =
    let
        vs =
            List.repeat (List.length rs) 0
    in
    writeRegisters rs vs



{- Instructions -}


nop : CPU -> CPU
nop cpu =
    { cpu | registers = writeTick cpu.registers }


{-| Adds E and A together, storing the result in A.
-}
addEA : CPU -> CPU
addEA cpu =
    let
        -- Perform addition
        sum =
            cpu.registers.e + cpu.registers.a

        -- Determine flags
        f =
            case ( Bitwise.and sum 255 == 0, sum > 255 ) of
                ( False, False ) ->
                    0

                -- 0x80
                ( True, False ) ->
                    128

                -- Bitwise.or 0x80 0x10
                ( True, True ) ->
                    144

                -- 0x10
                ( False, True ) ->
                    16

        -- Mask to 8 bits
        a =
            Bitwise.and sum 255

        -- Update registers and add a tick to the CPU clock.
        registers =
            writeRegisters [ A, F ] [ a, f ] <| writeTick cpu.registers
    in
    tick { cpu | registers = registers }


{-| Compares register A to register B.
-}
cprB : CPU -> CPU
cprB cpu =
    let
        i =
            cpu.registers.a - cpu.registers.b

        -- Substitution flag
        nf =
            Bitwise.or cpu.registers.f 0x40

        -- Determine flags
        f =
            case ( Bitwise.and i 255 == 0, i < 0 ) of
                ( False, False ) ->
                    nf

                ( True, False ) ->
                    Bitwise.or nf 0x80

                ( True, True ) ->
                    Bitwise.or nf (Bitwise.or 0x10 0x80)

                ( False, True ) ->
                    Bitwise.or nf 0x10

        registers =
            writeRegister ( F, f ) <| writeTick cpu.registers
    in
    tick { cpu | registers = registers }


{-| Preliminary MOV operation, for tinkering.
-}
mov : Register -> Int -> CPU -> CPU
mov r v cpu =
    { cpu | registers = writeRegister ( r, v ) <| writeTick cpu.registers }


{-| Opcode execution mapping
-}
execute : Opcode -> CPU -> CPU
execute opcode =
    case opcode of
        Nop ->
            nop

        AddEA ->
            addEA

        CpRB ->
            nop

        Mov r v ->
            mov r v
