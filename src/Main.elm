module Main exposing (Msg, init, main, subscriptions, update, view)

import Browser
import Browser.Events exposing (onAnimationFrame)
import Html exposing (Html, button, div, input, main_, table, tbody, td, text, th, thead, tr)
import Html.Attributes exposing (class, colspan, type_, value)
import Html.Events exposing (onClick, onInput)
import Time
import Z80 exposing (CPU, Clock, Opcode(..), Register(..), Registers)
import Z80.CPU


type alias Model =
    { cpu : CPU
    , a : String
    , e : String
    }


normalizeRegisterValue : String -> Int
normalizeRegisterValue v =
    case String.toInt v of
        Just n ->
            if n > 255 then
                255

            else if n < 0 then
                0

            else
                n

        Nothing ->
            0


init : () -> ( Model, Cmd Msg )
init _ =
    ( { cpu = Z80.initialCPU, a = "0", e = "0" }, Cmd.none )


type Msg
    = Tick Time.Posix
    | MovLocal Register
    | Operation Opcode
    | SetRegister Register String


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Tick _ ->
            ( { model | cpu = Z80.CPU.tick model.cpu }, Cmd.none )

        SetRegister r v ->
            let
                newModel =
                    case r of
                        A ->
                            { model | a = v }

                        E ->
                            { model | e = v }

                        _ ->
                            model
            in
            ( newModel, Cmd.none )

        MovLocal r ->
            let
                cpu =
                    case r of
                        A ->
                            Z80.CPU.execute (Mov A (normalizeRegisterValue model.a)) model.cpu

                        E ->
                            Z80.CPU.execute (Mov E (normalizeRegisterValue model.e)) model.cpu

                        _ ->
                            model.cpu
            in
            ( { model | cpu = cpu }, Cmd.none )

        Operation op ->
            ( { model | cpu = Z80.CPU.execute op model.cpu }, Cmd.none )


subscriptions : Model -> Sub Msg
subscriptions _ =
    --onAnimationFrame Tick
    Time.every 1000 Tick


view : Model -> Html Msg
view model =
    main_
        []
        [ viewTriggers model
        , div
            [ class "tables" ]
            [ div [ class "table-container" ] [ viewClock model.cpu.clock ]
            , div [ class "table-container" ] [ viewRegisters model.cpu.registers ]
            ]
        ]


operationButton : String -> Msg -> Html Msg
operationButton label msg =
    button
        [ onClick msg ]
        [ text label ]


viewTriggers : Model -> Html Msg
viewTriggers model =
    div
        [ class "triggers" ]
        [ button [ onClick <| Operation Nop ] [ text "Nop" ]
        , button [ onClick <| Operation AddEA ] [ text "AddEA" ]
        , button [ onClick <| MovLocal A ] [ text <| "Mov A," ++ model.a ]
        , input [ type_ "text", value model.a, onInput <| SetRegister A ] []
        , button [ onClick <| MovLocal E ] [ text <| "Mov E," ++ model.e ]
        , input [ type_ "text", value model.e, onInput <| SetRegister E ] []
        ]


viewClock : Clock -> Html Msg
viewClock clock =
    table
        []
        [ viewHeader "Clock"
        , tbody
            []
            [ viewRow "M" clock.m
            , viewRow "T" clock.t
            ]
        ]


viewRegisters : Registers -> Html Msg
viewRegisters registers =
    table
        []
        [ viewHeader "Registers"
        , tbody
            []
            [ viewRow "A" registers.a
            , viewRow "B" registers.b
            , viewRow "C" registers.c
            , viewRow "D" registers.d
            , viewRow "E" registers.e
            , viewRow "H" registers.h
            , viewRow "L" registers.l
            , viewRow "F" registers.f
            , viewRow "PC" registers.pc
            , viewRow "SP" registers.sp
            , viewRow "M" registers.m
            , viewRow "T" registers.t
            ]
        ]


viewHeader : String -> Html Msg
viewHeader h =
    thead
        []
        [ tr
            []
            [ th [ colspan 2 ] [ text h ] ]
        ]


viewRow : String -> Int -> Html Msg
viewRow r v =
    tr
        []
        [ td [] [ text r ]
        , td [] [ text <| String.fromInt v ]
        ]


main : Program () Model Msg
main =
    Browser.element
        { init = init
        , view = view
        , update = update
        , subscriptions = subscriptions
        }
