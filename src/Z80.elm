module Z80 exposing (..)

import Bitwise


type alias Clock =
    { m : Int
    , t : Int
    }


type Register
    = A
    | B
    | C
    | D
    | E
    | H
    | L
    | F
    | PC
    | SP
    | M
    | T


type alias Registers =
    { a : Int
    , b : Int
    , c : Int
    , d : Int
    , e : Int
    , h : Int
    , l : Int
    , f : Int
    , pc : Int
    , sp : Int
    , m : Int
    , t : Int
    }


type alias CPU =
    { clock : Clock
    , registers : Registers
    }


type Opcode
    = Nop
    | AddEA
    | CpRB
    | Mov Register Int


initialClock : Clock
initialClock =
    Clock 0 0


initialRegisters : Registers
initialRegisters =
    Registers 0 0 0 0 0 0 0 0 0 0 0 0


initialCPU : CPU
initialCPU =
    CPU initialClock initialRegisters
